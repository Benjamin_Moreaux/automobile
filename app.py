import csv

def lecturecsv(nomFichier):
    fileContent = ""
    with open(nomFichier, newline='') as csvfile :
        reader = csv.reader(csvfile, delimiter='|')
        fileContent = [row for row in reader]
    return fileContent

def ecriturecsv(nomFichier, content):
    nomFichiers = nomFichier[:len(nomFichier)-3] + "new.csv"
    with open("new.csv", 'w') as csvfile :
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerows(content)
        return True
    return False
    nomFichier = "new.csv"
    return nomFichier

fileContent = ""
nomFichier = "auto.csv"
lecturecsv(nomFichier)
ecriturecsv(nomFichier, fileContent)
with open(nomFichier, newline='') as csvfile:
    reader = csv.DictReader(csvfile,delimiter=';')
    for row in reader:
        print(row['address'], row['name'],row['firstname'],row['immat'],row['date_immat'],
        row['vin'],row['marque'],row['denominations'],row['couleur'],row['carrosserie'],
        row['categorie'],row['cylindree'],row['energy'],row['places'],row['poids'],
        row['puissance'],row['type_variante_version'])
